Source: ros-bond-core
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Build-Depends: debhelper-compat (= 13), catkin (>= 0.8.10-1~),
		ros-message-generation, libstd-msgs-dev,
		python3-all, python3-setuptools,
		ros-cmake-modules, libroscpp-dev, librosconsole-dev,
		libros-rosgraph-msgs-dev, ros-rosgraph-msgs,
		libxmlrpcpp-dev, uuid-dev,
		libroscpp-core-dev, libboost-dev,
		libboost-thread-dev,
		libroscpp-msg-dev, ros-roscpp-msg,
		libboost-filesystem-dev, libboost-regex-dev,
		dh-sequence-python3,
		libconsole-bridge-dev, librostest-dev, libgtest-dev, python3-rostest,
Standards-Version: 4.6.1
Section: libs
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/bond_core
Vcs-Git: https://salsa.debian.org/science-team/ros-bond-core.git
Vcs-Browser: https://salsa.debian.org/science-team/ros-bond-core

Package: libbond-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, ros-message-generation, libstd-msgs-dev
Description: Messages related to Robot OS bond_core - development
 This package is part of Robot OS (ROS). This is the 'bond' process state
 machine library development files.
 .
 Bond is a mechanism for checking when another process has
 terminated. A bond allows two processes, A and B, to know when the
 other has terminated, either cleanly or by crashing. The bond remains
 connected until it is either broken explicitly or until a heartbeat
 times out.

Package: python3-bond
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-genpy, python3-std-msgs
Description: Messages related to Robot OS bond_core - Python 3
 This package is part of Robot OS (ROS). This is the 'bond' process state
 machine library Python 3 bindings.
 .
 Bond is a mechanism for checking when another process has
 terminated. A bond allows two processes, A and B, to know when the
 other has terminated, either cleanly or by crashing. The bond remains
 connected until it is either broken explicitly or until a heartbeat
 times out.

Package: cl-bond
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}, cl-std-msgs
Description: Messages related to Robot OS bond_core - LISP
 This package is part of Robot OS (ROS). This is the 'bond' process state
 machine library LISP bindings.
 .
 Bond is a mechanism for checking when another process has
 terminated. A bond allows two processes, A and B, to know when the
 other has terminated, either cleanly or by crashing. The bond remains
 connected until it is either broken explicitly or until a heartbeat
 times out.

Package: libsmc-dev
Section: libdevel
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Robot OS 'bond' State Machine Compiler interface
 The State Machine Compiler (SMC) from http://smc.sourceforge.net/
 converts a language-independent description of a state machine
 into the source code to support that state machine.
 .
 This package contains the libraries that a compiled state machine
 depends on, but it does not contain the compiler itself.

Package: python3-smclib
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Robot OS 'bond' State Machine Compiler Python 3 package
 The State Machine Compiler (SMC) from http://smc.sourceforge.net/
 converts a language-independent description of a state machine
 into the source code to support that state machine.
 .
 This package contains the Python 3 interface.

Package: libbondcpp-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libbondcpp1d (= ${binary:Version}), uuid-dev, ${misc:Depends}, libbond-dev, libroscpp-dev, libsmc-dev, ros-cmake-modules
Description: Development files for Robot OS bondcpp library
 This package is part of Robot OS (ROS). It is the 'bond' process state
 machine library C++ implementation development files.
 .
 Bond is a mechanism for checking when another process has
 terminated. A bond allows two processes, A and B, to know when the
 other has terminated, either cleanly or by crashing. The bond remains
 connected until it is either broken explicitly or until a heartbeat
 times out.

Package: libbondcpp1d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Robot OS bondcpp library
 This package is part of Robot OS (ROS). It is the 'bond' process state
 machine library C++ implementation.
 .
 Bond is a mechanism for checking when another process has
 terminated. A bond allows two processes, A and B, to know when the
 other has terminated, either cleanly or by crashing. The bond remains
 connected until it is either broken explicitly or until a heartbeat
 times out.

Package: python3-bondpy
Section: python
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-bond, python3-rospy, python3-smclib
Description: Python 3 implementation of bond
 This package is part of Robot OS (ROS). It is the 'bond' process state
 machine library Python 3 implementation.
 .
 Bond is a mechanism for checking when another process has terminated. A
 bond allows two processes, A and B, to know when the other has terminated,
 either cleanly or by crashing. The bond remains connected until it is either
 broken explicitly or until a heartbeat times out. This package contains
 the Python 3 package.
